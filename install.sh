#!/bin/bash

# Crea los directorios necesarios
mkdir -p public/assets

# Copia los archivos y directorios
cp -r assets public
cp style.css public
cp index.html public
cp bootstrap-lux.css public